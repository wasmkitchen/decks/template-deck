# template-deck

## ttyd

https://tsl0922.github.io/ttyd/
```bash
brew install ttyd
```

```bash
ttyd --port 8000 bash
```

```bash
ttyd --port 8001 zellij
```

## Slides (MARP)
> https://github.com/marp-team/marp-cli
```bash
brew install marp-cli
```
