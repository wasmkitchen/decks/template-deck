---
size: 16:9
paginate: true
---
<style>
h1 {
    color: #21223C;
    text-align: left;
}
h2 {
    color: #212F3C;
    text-align: left;
}
h3 {
    color: #2E213C;
    text-align: left;
}

iframe {
    border: none;
}
</style>
# 🦊 Everything should start with an issue

Philippe Charrière CSE @ GitLab

<h1>
hello
</h1>

<script>
    console.log("hello")
</script>
---
# 🚀 demo

<iframe id="demo"
    title="demo"
    width="100%"
    height="80%"
    src="http://localhost:8000">
</iframe>

`this is a demo`

<!-- 
Some notes here that might be useful.
-->
---
# 🚀 demo

<iframe id="demo"
    title="demo"
    width="100%"
    height="80%"
    src="http://localhost:8000">
</iframe>

`this is a demo`

<!-- 
Some notes here that might be useful.
-->
---
<!-- _class: invert -->
<!-- _color: "#FFFFFF" -->
# Section

---
